<?php

namespace App\Entity;

class User
{
  public $blogname;
  public $name;
  public $surname;
  public $mail;
  public $birthdate;
  public $password;

  public function __construct(string $blogname, string $name, string $surname, string $mail, string $birthdate)
  {
    $this->blogname = $blogname;
    $this->name = $name;
    $this->surname = $surname;
    $this->mail = $mail;
    $this->birthdate = $birthdate;
    $this->password = $password;
  }
}