<?php

namespace App\Entity;


class Article
{
  public $id;
  public $title;
  public $content;
  public $author;
  public $date;
   public $url;

  public function __construct(string $title = null, string $content = null, int $id = null, string $author = null, \DateTime $date = null)
  {
    $this->id = $id;
    $this->title = $title;
    $this->content = $content;
    $this->author = $author;
    $this->date = $date;
     //$this->url = $url;
  }

  public function fromSQL(array $sql)
  {
    $this->id = $sql["id"];
    $this->title = $sql["title"];
    $this->content = $sql["content"];
    $this->author = $sql["author"];
    $this->date = new \DateTime($sql["date"]);
    $this->url = $sql["url"];
  }

}
