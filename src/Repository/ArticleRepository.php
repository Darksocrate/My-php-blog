<?php

namespace App\Repository;

use App\Entity\Article;
use App\Controller\ArticleController;
use App\Util\ConnectUtil;


class ArticleRepository
{
    public function getAll() : array
    {
        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");
        $articles = [];
        try {
            $cnx = ConnectUtil::getConnection();
            
            $query = $cnx->prepare("SELECT * FROM article ORDER BY id DESC");
           
            $query->execute();

            foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function add(Article $article)
    {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("INSERT INTO article (title, content, author, date, url) VALUES (:title, :content, :author, :date, :url)");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":content", $article->content);
            $query->bindValue(":author", $article->author);
            $query->bindValue(":date", $article->date->format("Y/m/d"));
            $query->bindValue(":url", $article->url);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }
 
    public function update(Article $article) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("UPDATE article SET title=:title, content=:content, author=:author, date=:date, url=:url WHERE id=:id");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":content", $article->content);
            $query->bindValue(":author", $article->author);
            $query->bindValue(":id", $article->id);
            $query->bindValue(":date", $article->date->format("Y/m/d"));
            $query->bindValue(":url", $article->url);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id) {
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("DELETE FROM article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id): ?Article{
        try {
            $cnx = ConnectUtil::getConnection();

            $query = $cnx->prepare("SELECT * FROM article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }
}