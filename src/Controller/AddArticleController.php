<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\File\File;

class AddArticleController extends AbstractController
{
    /**
     * @Route("/add-article", name="add-article")
     */

    public function index(Request $request, ArticleRepository $repo)
    {

        $form = $this->createForm(ArticleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $article=$form->getData();
            $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();
            $article->url->move(dirname(__FILE__)."/../../public/uploads", $fileName);
            $article->url = "uploads/".$fileName;
            $repo->add($article);


            return $this->redirectToRoute("home");
        }


        return $this->render('add-article.html.twig', [
            'form' => $form->createView(), 'article'=>$repo
        ]);
    }

    private function generateUniqueFileName()
    {
        return md5(uniqid());
    }
  

    /**
     * @Route("/add-article/remove/{id}", name="remove_article")
     */

    public function remove(int $id, ArticleRepository $repo)
    {
        $repo->delete($id);
        return $this->redirectToRoute("home");
    }

    /**
     * @Route("/add-article/update/{id}", name="update_article")
     */

    public function update(Request $request, int $id, ArticleRepository $repo)
    {
        $article = $repo->getById($id);

        $article->url = new File($article->url);
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {


            $article=$form->getData();
            $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();
            $article->url->move(dirname(__FILE__)."/../../public/uploads", $fileName);
            $article->url = "uploads/".$fileName;

            
            $repo->update($form->getData());

            return $this->redirectToRoute('home');
        }
        return $this->render('update-article.html.twig', [
            'form' => $form->createView()
        ]);
    }
}