<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
  /**
   * @Route("/home", name="home")
   */

  public function index(ArticleRepository $repo)
  {
    $result = $repo->getAll();
    return $this->render("home.html.twig", [
      'controller_name' => 'HomeController',
            'result'  => $result
    ]);
  }
}