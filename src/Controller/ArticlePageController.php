<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\File\File;

class ArticlePageController extends AbstractController
{
  /**
   * @Route ("/article-page/{id}",name="article-page")
   */

  public function index(int $id, ArticleRepository $repo)
  {
    $article = $repo->getById($id);

    // $article->url = new File($article->url, false);

    // $form = $this->createForm(AddArticleType::class, $article);

  //   if ($form->isSubmitted() && $form->isValid()) {

  //     $fileName = $this->generateUniqueFileName().'.'.$article->url->guessExtension();
  //     $article->url->move(dirname(__FILE__)."/../../public/uploads", $fileName);
  //     $article->url = "uploads/".$fileName;
  //     $repo->update($form->getData());

  //     return $this->redirectToRoute("home");
  // }


    return $this->render('article-page.html.twig', [
      'article' => $article,
    ]);
  }

  private function generateUniqueFileName()
  {
      return md5(uniqid());
  }

}


