/* global ScrollReveal $ */

const sr = ScrollReveal()

sr.reveal('h5', {
  origin: 'top',
  duration: 1100
})
sr.reveal('h2', {
  origin: 'top',
  duration: 1100
})
sr.reveal('h1', {
  origin: 'top',
  duration: 1100
})
sr.reveal('h3', {
  origin: 'top',
  duration: 1100
})
sr.reveal('h4', {
  origin: 'top',
  duration: 1100
})
sr.reveal('p', {
  origin: 'top',
  duration: 1100
})
sr.reveal('i', {
  origin: 'right',
  duration: 1000,
  interval: 200
})
sr.reveal('hr', {
  origin: 'left',
  duration: 1700
})
sr.reveal('#article-content', {
  origin: 'bottom',
  duration: 1500
})

$('.carousel').carousel({
  interval: 4000
})
