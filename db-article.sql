CREATE DATABASE  IF NOT EXISTS `db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db`;
-- MySQL dump 10.13  Distrib 5.7.22, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: db
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.34-MariaDB-1~jessie

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(45) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (11,'Humanity #5','Sony\'s reveal of Ghost of Tsushima was one of the highlights of Paris Games Week 2017, and what a surprise it was to find out that Infamous and Sly Cooper developer, Sucker Punch Games, is behind the project. The original trailer laid the foundation: a fictionalized take on an actual 13th century Mongol invasion, with a protagonist who transforms from samurai to ninja in order to protect the island his people call home. This year\'s E3 gave us a first look at live gameplay, and our first real taste of the beautifully realized environments that serve as the backdrop for what promises to be a brutal and unforgiving conflict.','Ligonnet Florian','2023-10-05 00:00:00'),(12,'Venom','Sony Pictures showed a few more scenes of the symbiote itself in action during their Venom presentation at San Diego Comic-Con on Friday. The footage didn\'t feature finished visual effects but it did show off the creature\'s size, ferocity, and his interactions with Eddie Brock as well as his dark wit.\r\n\r\nThe footage began with Eddie meditating while listening to self-help audio, which instructed him that while he couldn\'t change what happened in the past, he can be in total control in the present. We then see the effects of the symbiote taking him over, some of which we\'ve already seen in the trailers where he complains of feeling sick.','Ligonnet Florian','2018-07-25 00:00:00'),(14,'DC Comics / Marvel','For the sake of comparison, Netflix\'s Basic streaming plan for one screen in Standard Definition is also $7.99 a month; its Standard plan is $10.99 for two screens and high-definition streaming; and its Premium plan, with four screens and Ultra HD included, is $13.99 a month. Hulu\'s packages range from $7.99 for a Limited Commercials plan, $11.99 for No Commercials, $39.99 for Hulu with Live TV and $43.99 for Hulu\'s No Commercials plan plus Live TV. Amazon Prime is $12.99 a month/$119 per year, while Prime Video membership without other Prime perks is $8.99 a month.\r\n\r\nWhile Netflix is also branching out into digital comics distribution, and Amazon owns Comixology, neither service has integrated its TV shows, movies, comic books and community forums together yet, which theoretically allows DC Universe to get a head start on creating a curated viewing experience that can combine all of a fan\'s favorite properties in one place, regardless of their medium.','Ligonnet Florian','2020-05-16 00:00:00'),(15,'Marvel','While Netflix is also branching out into digital comics distribution, and Amazon owns Comixology, neither service has integrated its TV shows, movies, comic books and community forums together yet, which theoretically allows DC Universe to get a head start on creating a curated viewing experience that can combine all of a fan\'s favorite properties in one place, regardless of their medium.','Ligonnet Florian','2016-05-30 00:00:00'),(17,'Movies','DC has also remastered its backlist inventory in 4K resolution and developed its own native comic reader that has been optimized for every platform the service will be available on at launch - including iOS, Android, Roku, Apple TV, Amazon Fire TV, and Android TV, as well as the web and mobile web - which will allow each comic to be presented in whatever format best fits the screen it\'s being viewed on. Kamphausen and Lee demonstrated this on a widescreen TV, with Kamphausen admitting that being able to see the comic in 4K actually allowed him to notice small details he\'d missed when reading the comic in its traditional format the first time. Lee also pointed out that by presenting a comic on a big screen, reading it could become a more communal experience, with families or friends now able to experience a story at the same time.','Ligonnet Florian','2021-05-05 00:00:00');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-26 14:18:48
